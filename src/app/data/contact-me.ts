import { ContactMe } from "../models/contact-me";

export const CONTACT_ME: ContactMe[] = [
    {
        platform: "EMail",
        username: "info@bauer-jakob.de",
        imageUrl: "../../../../assets/images/home/contact-me/mail.svg",
        linkUrl: "mailto:info@bauer-jakob.de"
    },
];